package com.company;

import java.util.Random;

public class Sorteio {
    private int faceSorteada;

    public int Sortear(int numeroFaces) {
        Random random = new Random();
        faceSorteada = 1 + random.nextInt(numeroFaces);
        return faceSorteada;
    }
}
